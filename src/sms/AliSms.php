<?php

namespace Yufeng\MdkSdk\sms;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use think\facade\Log;
use Yufeng\MdkSdk\code\Code;
use Yufeng\MdkSdk\redis\BaseRedis;

class AliSms
{
    protected $sms_config = '';
    //验证码长度
    protected $len = '';

    //protected $sm_region_id = 'cn-hangzhou';

    public function __construct($len = 4, $config = [])
    {
        if($config !=[]) {
            $this->sms_config = $config;
        }
        $this->len = $len;
    }

    /**
     * 阿里云短信验证码发送
     * @param string $phone
     * @param string $template
     * @return bool|mixed
     * @throws ClientException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function sendCode(string $phone, string $template)
    {
        $code = Code::getCode($this->len);
        AlibabaCloud::accessKeyClient($this->sms_config['key_id'], $this->sms_config['key_secret'])->regionId($this->sms_config['region_id'])->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => $this->sms_config['region_id'],
                        'SignName' => $this->sms_config['sign_name'],
                        'PhoneNumbers' => $phone,
                        'TemplateCode' => $template,
                        'TemplateParam' => json_encode(['code' => $code]),
                    ],
                ])->request();
            Log::write('【' . $phone . '】短信验证码发送结果：' . json_encode($result));
            BaseRedis::setKey('sms:login:' . $phone, $code, 300);
            return true;
        } catch (ClientException $e) {
            Log::write('【' . $phone . '】短信验证码发送错误：' . json_encode($e));
            return false;
        } catch (ServerException $e) {
            Log::write('【' . $phone . '】短信验证码发送错误：' . json_encode($e));
            return false;
        }
        return false;
    }
}