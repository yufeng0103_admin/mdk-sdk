<?php

namespace Yufeng\MdkSdk\sms;

use TencentCloud\Common\Credential;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Sms\V20190711\Models\SendSmsRequest;
use TencentCloud\Sms\V20190711\SmsClient;
use think\facade\Log;
use Yufeng\MdkSdk\code\Code;
use Yufeng\MdkSdk\redis\BaseRedis;

class TxSms
{
    // 腾讯云账户秘密ID，注意不是短信中的id
    protected $sms_secretid = '';
    // 腾讯云账户中的秘钥key
    protected $sms_secretkey = '';
    // 腾讯云短信应用appid。应用管理 > 应用列表中的应用id
    protected $sms_appid = '';
    // 模板签名内容。一定是要 国内短信 > 签名管理 > 中的签名内容
    protected $sms_sign_name = '';
    protected $len = 4;
    protected $sms_config = '';
    /**
     * 腾通短信发送
     * TxSms constructor.
     * @param int $len 验证码长度
     * @param array $config 配置：secretid，secretkey，secrappidetid，sign_name
     */
    public function __construct($len = 4, $config = [])
    {
        if ($config != []) {
            $this->sms_config = $config;
            $this->sms_secretid = $config['secretid'];
            $this->sms_secretkey = $config['secretkey'];
            $this->sms_appid = $config['secrappidetid'];
            $this->sms_sign_name = $config['sign_name'];
            $this->len = $len;
        }
    }

    /**
     * 发送短信
     * @param string $phone 手机号
     * @param string $template 模板ID
     * @param string $redis_ex 缓存前缀 默认格式sms:login: 代表登录
     * @return bool
     */
    public function sendCode(string $phone, string $template, $redis_ex = 'sms:login:')
    {
        $SecretId = $this->sms_config['secretid']; // 腾讯云账户秘密ID，注意不是短信中的id
        $SecretKey = $this->sms_config['secretkey']; // 腾讯云账户中的秘钥key
        $AppID = $this->sms_config['secrappidetid']; // 腾讯云短信应用appid。应用管理 > 应用列表中的应用id
        $smsSign = $this->sms_config['sign_name']; // 模板签名内容。一定是要 国内短信 > 签名管理 > 中的签名内容
        $templateId = $template;// 正文模板中的模板id。 国内短信 > 正文模板管理 > 模板ID
        $expire_time = 600; //失效时间
        $wait_time = 120; //发送间隔秒数
        $redis_key = $phone;
        $phone = "+86" . $phone;
        $TemplateParamSet = Code::getCode($this->len);
        try {
            // 实例化认证对象
            $cred = new Credential($SecretId, $SecretKey);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("sms.tencentcloudapi.com");
            // 实例化一个 http 选项
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            // 实例化一个 client 选项
            $client = new SmsClient($cred, "", $clientProfile);
            // 实例化一个 sms 发送短信请求对象，每个接口都会对应一个 request 对象。
            $req = new SendSmsRequest();
            // 发送短信验证码配置参数
            $params = '{"PhoneNumberSet":["' . $phone . '"],"TemplateID":"' . $templateId . '", "Sign":"' . $smsSign . '","TemplateParamSet":["' . $TemplateParamSet . '"],"SmsSdkAppid":"' . $AppID . '"}';
            $req->fromJsonString($params);
            // 通过 client 对象调用 SendSms 方法发起请求
            $resp = $client->SendSms($req);
            if ($resp->SendStatusSet[0]->Code != 'Ok') {
                return false;
            }
            BaseRedis::setKey($redis_ex . $redis_key, $TemplateParamSet, 300);
            return true;
        } catch (TencentCloudSDKException $e) {
            Log::write('【' . $redis_key . '】短信验证码发送错误：' . json_encode($e));
            return false;
        }
        return false;
    }
}