<?php

namespace Yufeng\MdkSdk\cross;

use think\facade\Log;
use think\Response;

class Cross
{
    /**
     * @param $request
     * @param \Closure $next
     * @return mixed|void
     */
    public function handle($request, \Closure $next)
    {
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods: GET, POST,OPTIONS, PATCH, PUT, DELETE');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With,Origin,token');

        if (strtoupper($request->method()) == "OPTIONS") {
            header('Access-Control-Allow-Headers:token,Origin,X-Requested-With,Content-Type,content-type,Accept,Authorized-Token,Authori-zation,Authorization,authorized-token');
            header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE,OPTIONS,PATCH');
            exit();
        }

        return $next($request);
    }
}