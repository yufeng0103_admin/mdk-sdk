<?php

namespace Yufeng\MdkSdk\redis;

use think\facade\Cache;

/**
 * Redis操作类
 *  多应用需配置 redis选择数据库'redis_db' => 'api', //缓存前缀 cache_prefix' => 'api'
 * Class BaseRedis
 * @package app\common\lib\redis
 */
class BaseRedis
{
    const db = 'redis';

    /**
     * 连接redis
     * @param $db_name
     * @return object|null
     */
    public static function getRedis($db_name = '')
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        if ($db_name != '') {
            $redis_db = $db_name;
        }
        return Cache::store($redis_db)->handler();
    }

    /**
     * key 是否存在
     * @param $key
     * @return mixed
     */
    public static function exists($key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->exists(config('config.cache_prefix') . ':' . $key);
    }

    /**
     * 设置生存时间
     * @param $key
     * @param $secondes
     * @return mixed
     */
    public static function expire($key, $secondes)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->expire(config('config.cache_prefix') . ':' . $key, $secondes);
    }

    /**
     * desc:设置 string key
     * @param $redis_key
     * @param $data
     * @param  $cache_time
     * @return mixed
     */
    public static function setKey($redis_key, $data, $cache_time = 0)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        if (!$cache_time) {
            return static::getRedis($redis_db)->set(config('config.cache_prefix') . ':' . $redis_key, $data);
        } else {
            return static::getRedis($redis_db)->set(config('config.cache_prefix') . ':' . $redis_key, $data, $cache_time);
        }

    }

    /**
     * incr:加一操作
     * @param $redis_key
     * @return mixed
     */
    public static function incr($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->incr(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * incrBy:原子加操作
     * @param $redis_key
     * @param $value
     * @param string $db
     * @return mixed
     */
    public static function incrBy($redis_key, $value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->incrBy(config('config.cache_prefix') . ':' . $redis_key, $value);
    }

    /**
     * desc:获取 string key
     * @param $redis_key
     * @param string $db_name
     * @return mixed
     */
    public static function getKey($redis_key, $db_name = '')
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        if ($db_name != '') {
            $redis_db = $db_name;
            $rs = static::getRedis($redis_db)->get($redis_key);
        } else {
            $rs = static::getRedis($redis_db)->get(config('config.cache_prefix') . ':' . $redis_key);
        }
        return $rs;
    }

    /**
     * 插入到列表表位(最右边)
     * @param $redis_key
     * @param array $value
     * @return mixed
     */
    public static function rPush($redis_key, array $value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->rPush(config('config.cache_prefix') . ':' . $redis_key, ...$value);
    }

    /**
     * 插入到列表表位(最左边)
     * @param $redis_key
     * @param array $value
     * @return mixed
     */
    public static function lPush($redis_key, array $value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->lPush(config('config.cache_prefix') . ':' . $redis_key, ...$value);
    }

    /**
     * 移除列表的最后一个元素，返回值为移除的元素(最右边)
     * @param $redis_key
     * @param string $db
     * @return mixed
     */
    public static function rPop($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->rPop(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * 移除列表的最后一个元素，返回值为移除的元素(最左边)
     * @param $redis_key
     * @return mixed
     */
    public static function lPop($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->lPop($redis_key);
    }

    /**
     * 從列表左側pop多個元素
     * @param $redis_key
     * @param $n
     * @return mixed
     */
    public static function lPopRange($redis_key, $n)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        $ret = static::getRedis($redis_db)->multi()->lrange(config('config.cache_prefix') . ':' . $redis_key, 0, $n - 1)->ltrim(config('config.cache_prefix') . ':' . $redis_key, $n, -1)->exec();

        return $ret[0];
    }

    /**
     * 從列表右側pop多個元素
     * @param $redis_key
     * @param $n
     * @param string $db
     * @return mixed
     */
    public static function rPopRange($redis_key, $n)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        $ret = static::getRedis($redis_db)->multi()->lrange(config('config.cache_prefix') . ':' . $redis_key, -$n, -1)->ltrim(config('config.cache_prefix') . ':' . $redis_key, 0, -$n - 1)->exec();

        return $ret[0];
    }

    /**
     * @param $redis_key
     * @param $index
     * @return mixed
     */
    public static function lIndex($redis_key, $index)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->lIndex(config('config.cache_prefix') . ':' . $redis_key, $index);
    }


    /**
     * 获取List长度
     * @param $redis_key
     * @param string $db
     * @return mixed
     */
    public static function lLen($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->lLen(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * 获取hash中的某个key的值
     * @param $redis_key
     * @param $hash_key
     * @param string $db
     * @return mixed
     */
    public static function hGetKey($redis_key, $hash_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hGet(config('config.cache_prefix') . ':' . $redis_key, $hash_key);
    }

    /**
     * 获取hash的长度
     * @param $redis_key
     * @return mixed
     */
    public static function hLen($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hLen(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * desc:批量设置 hash key
     * @param $redis_key
     * @param array $data
     * @param int $cache_time
     * @return array
     */
    public static function hmSetKey($redis_key, array $data, $cache_time = 0)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        $handler = static::getRedis($redis_db);
        $handler->hMset(config('config.cache_prefix') . ':' . $redis_key, $data);
        $handler->expire(config('config.cache_prefix') . ':' . $redis_key, $cache_time);
        return $data;
    }

    /**
     * 单独设置hash key
     * @param $redis_key
     * @param $hash_key
     * @param $data
     * @return mixed
     */
    public static function hSetKey($redis_key, $hash_key, $data)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hSet(config('config.cache_prefix') . ':' . $redis_key, $hash_key, $data);
    }

    /**
     * 添加集合成员
     * @param $redis_key
     * @param mixed ...$value
     * @return mixed
     */
    public static function sAdd($redis_key, ...$value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->sAdd(config('config.cache_prefix') . ':' . $redis_key, ...$value);
    }

    /**
     * 删除集合成员
     * @param $redis_key
     * @param mixed ...$value
     * @return mixed
     */
    public static function sRem($redis_key, ...$value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->sRem(config('config.cache_prefix') . ':' . $redis_key, ...$value);
    }

    /**
     * 是否存在集合中
     * @param $redis_key
     * @param $value
     * @param string $db
     * @return mixed
     */
    public static function sIsMember($redis_key, $value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->sIsMember(config('config.cache_prefix') . ':' . $redis_key, $value);
    }

    /**
     * 获取集合内容
     * @param $redis_key
     * @return mixed
     */
    public static function sMembers($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->sMembers(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * 查询集合长度
     * @param $redis_key
     * @param string $db
     * @return mixed
     */
    public static function sCard($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->sCard(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * 删除hash key
     * @param $redis_key
     * @param array $hash_keys
     * @return mixed
     */
    public static function hDelKey($redis_key, array $hash_keys)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hDel(config('config.cache_prefix') . ':' . $redis_key, ...$hash_keys);
    }

    /**
     * desc:获取 hash key
     * @param $redis_key
     * @return mixed
     */
    public static function hGetAllKey($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hgetall(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * hash 累加操作
     * @param $key
     * @param $hashKey
     * @param $value
     * @return mixed
     */
    public static function hIncrBy($key, $hashKey, $value)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->hIncrBy(config('config.cache_prefix') . ':' . $key, $hashKey, $value);
    }

    /**
     * desc:删除 hash key
     * @param $redis_key
     * @return mixed
     */
    public static function delKey($redis_key)
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->del(config('config.cache_prefix') . ':' . $redis_key);
    }

    /**
     * 刷新所有缓存 测试用,线上禁止调用
     * @return mixed
     */
    public static function reFlash()
    {
        $redis_db = self::db;
        if (config('config.redis_db') != '') {
            $redis_db = config('config.redis_db');
        }
        return static::getRedis($redis_db)->flushAll();
    }
}