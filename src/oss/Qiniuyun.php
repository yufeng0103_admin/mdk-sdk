<?php
namespace Yufeng\MdkSdk\oss;


use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class  Qiniuyun
{
    protected $accessKey = '';
    protected $secretKey = '';
    protected $bucket = '';

    /**
     * Qiniuyun constructor.
     * @param $base_param 传入传输$base_param['accessKey'] 、 $base_param['secretKey']、$base_param['bucket'](上传空间名称)
     */
    public function __construct($base_param)
    {
        $this->accessKey = $base_param['accessKey'];
        $this->secretKey = $base_param['secretKey'];
        $this->bucket = $base_param['bucket'];
    }

    /**
     * 上传
     * @param $file
     * @param $file_name
     * @return mixed
     * @throws \Exception
     */
    public function execute($file, $file_name)
    {

        $auth = new Auth($this->accessKey, $this->secretKey);
        // 生成上传Token
        $token = $auth->uploadToken($this->bucket);

        // 要上传文件的本地路径
        $filePath = $file; //你接收的图片
        // 上传到存储后保存的文件名
        $key = $file_name;
        // 初始化 UploadManager 对象并进行文件的上传。
        // 构建 UploadManager 对象
        $uploadMgr = new UploadManager();

        // 调用 UploadManager 的 putFile 方法进行文件的上传。
        list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);

        //根据需求加入自己的逻辑处理
        if ($err !== null) {
            return ['code' => 1, 'data' => $err];
        } else {
            return ['code' => 0, 'data' => $ret];
        }

    }


}