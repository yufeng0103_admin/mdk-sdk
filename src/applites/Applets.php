<?php

namespace Yufeng\MdkSdk\applets;

use think\facade\Log;
use Yufeng\MdkSdk\curl\CurlBase;

/**
 * 微信小程序相关操作
 * Class Applets
 */
class Applets
{
    //小程序APPID
    protected $appid = '';
    //小程序appsecret
    protected $appsecret = '';
    //获取AccessToken请求地址
    protected $api_access_token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&';
    //获取小程序打开路径请求地址
    protected $api_generatescheme = 'https://api.weixin.qq.com/wxa/generatescheme?access_token=';
    //请求发送模板消息请求
    protected $api_send_message = 'https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=';

    /**
     * Applets constructor.
     * @param $appid 小程序Appid
     * @param $appsecret 小程序AppSecret
     */
    public function __construct($appid, $appsecret)
    {
        $this->appid = $appid;
        $this->appsecret = $appsecret;
    }

    /**
     * 动态生成带参数小程序打开路径
     * @param array $param 传入参数
     * @param string $path 打开路径
     * @return bool|string  返回结果样例：{    "errcode": 0,    "errmsg": "ok",    "openlink": Scheme,    }结果要取openlink
     * @throws ErrorException
     */
    public function getUrlScheme($path = '', $param = [])
    {
        $access_token_res = $this->getAccessToken();
        $access_token = json_decode($access_token_res, true);
        $post_api_url = $this->api_generatescheme . $access_token['access_token'];
        $url = '';
        if ($path == '') {
            if ($param == []) {
                $post_parms = array(
                    'expire_type' => 0,
                    'jump_wxa' => array(
                        'path' => '/pages/index/index',
                        'query' => ''
                    )
                );
            } else {
                $post_parms = array(
                    'expire_type' => 0,
                    'jump_wxa' => array(
                        'path' => '/pages/index/index',
                        'query' => http_build_query($param)
                    )
                );
            }
        } else {
            if ($param == []) {
                $post_parms = array(
                    'expire_type' => 0,
                    'jump_wxa' => array(
                        'path' => $path,
                        'query' => ''
                    )
                );
            } else {
                $post_parms = array(
                    'expire_type' => 0,
                    'jump_wxa' => array(
                        'path' => $path,
                        'query' => http_build_query($param)
                    )
                );
            }
        }

        try {
            $return = CurlBase::post($post_api_url, $post_parms, 'JSON');
            $return = json_decode($return);
            return $return->openlink;
        } catch (\Exception $e) {
            echo $e->getMessage();
            Log::record("获取取小程序打开路径失败！" . $e->getMessage() . __FILE__ . __LINE__);
            return $e->getMessage() . __FILE__ . __LINE__;
            die();
        }
    }

    /**
     * 根据code获取用户OpenId
     * @param $code
     * @return bool|string
     */
    public function getOpenid($code)
    {
        $url = 'https://api.weixin.qq.com/sns/jscode2session';
        $param = 'appid=' . $this->appid . '&secret=' . $this->appsecret . '&grant_type=authorization_code&js_code=' . $code;
        $res = CurlBase::get($url . '?' . $param);
        return $res;

    }

    /**
     * 获取AccessToken
     * config配置参数：
     */
    private function getAccessToken()
    {
        try {
            $acc_token = CurlBase::get($this->api_access_token_url . 'appid=' . $this->appid . '&secret=' . $this->appsecret);
            Log::write("获取AccessToken:" . json_encode($acc_token) . '[' . __FILE__ . __LINE__ . ']');
            return $acc_token;
        } catch (\Exception $e) {
            Log::record("获取AccessToken失败！" . $e->getMessage() . '[' . __FILE__ . __LINE__ . ']');
            throw new ErrorException(1, $e->getMessage(), __FILE__, __LINE__);
            return $e->getMessage() . __FILE__ . __LINE__;
            die();
        }
    }

    /**
     * 发送模板消息
     * @param $data 模板消息
     * @param $template_id 模板ID
     * @param $touser 推送给谁 OpenID
     * @param $page 打开界面
     * @param string $lang
     * @return bool|string
     * @throws ErrorException
     */
    public function sendMessage($data, $template_id, $touser, $page, $lang = 'zh_CN')
    {
        $accessToken = $this->getAccessToken();
        $accessToken = json_decode($accessToken, true);
        $post_url = $this->api_send_message . $accessToken['access_token'];
        $postData = [
            'touser' => $touser,
            'template_id' => $template_id,
            'page' => $page,
            'data' => $data
        ];
        Log::write('模板消息请求接口接口：' . $post_url . PHP_EOL);
        Log::write('模板消息接口提交数据：' . json_encode($postData, JSON_UNESCAPED_UNICODE) . PHP_EOL);
        return CurlBase::post($post_url, $postData, 'WX_Appletes');
    }


}