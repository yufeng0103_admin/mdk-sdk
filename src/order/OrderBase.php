<?php

namespace Yufeng\MdkSdk\order;

class OrderBase
{
    /**
     * 根据前缀获取订单号
     * @param string $exp_str 订单号前缀 默认No
     * @return string
     */
    public static function createOrderNo($exp_str = 'No')
    {
        $code = mt_rand('100000', 999999);
        return $exp_str . $code . strtotime("now");
    }
}
